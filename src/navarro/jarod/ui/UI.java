package navarro.jarod.ui;

import java.io.*;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import navarro.jarod.logica.tl.Controller;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {
        boolean noSalir = true;
        do {
            menu();
            out.println("Digite la opción que guste");
            int opcion = Integer.parseInt(in.readLine());
            noSalir = ejecutarAccion(opcion);
        } while (noSalir);
    }

    public static void menu() throws IOException {
        out.println();
        out.println("1. Registrar Organización");
        out.println("2. Registrar Persona Fisica");
        out.println("3. Registrar Cliente");
        out.println("4. Registrar Producto");
        out.println("5. Listar Proveedores");
        out.println("6. Listar Clientes");
        out.println("7. Listar Productos");
        out.println("8. Realizar compra en mi carrito");
        out.println("9. Listar carrito de compras");
        out.println("10. Salir");
    }

    public static boolean ejecutarAccion(int opcion) throws IOException {
        boolean noSalir = true;
        switch (opcion) {
            case 1:
                registrarOrganizacion();
                break;
            case 2:
                registrarPersonaFisica();
                break;
            case 3:
                registrarCliente();
                break;
            case 4:
                registrarProducto();
                break;
            case 5:
                listarProveedores();
                break;
            case 6:
                listarClientes();
                break;
            case 7:
                listarProductos();
                break;
            case 8:
                carritoDeCompras();
                break;
            case 9:
                listarCarritoCompras();
                break;
            case 10:
                noSalir = false;
                out.println("Gracias por usar el programa!");
                break;
            default:
                out.println("Digite una opción correcta");
                break;

        }
        return noSalir;
    }

    public static void registrarOrganizacion() throws IOException {
        String nombre;
        String direccion;
        String telefono;
        String id;
        String representante;

        out.println("Digite el ID");
        id = in.readLine();
        if (gestor.proovedorExiste(id)) {
            out.println("El proveedor ya existe");
        } else {
            out.println("Digite el nombre de la organización");
            nombre = in.readLine();
            out.println("Digite la dirección");
            direccion = in.readLine();
            out.println("Digite el telefono");
            telefono = in.readLine();
            out.println("Digite el nombre del representante");
            representante = in.readLine();

            gestor.registrarOrganización(nombre, direccion, telefono, id, representante);
        }
    }

    public static void registrarPersonaFisica() throws IOException {
        String nombre;
        String direccion;
        String telefono;
        String id;
        String apellidos;

        out.println("Digite el id");
        id = in.readLine();
        if (gestor.proovedorExiste(id)) {
            out.println("El proveedor ya existe");
        } else {
            out.println("Digite el nombre de la persona fisica");
            nombre = in.readLine();
            out.println("Digite la dirección");
            direccion = in.readLine();
            out.println("Digite el telefono");
            telefono = in.readLine();
            out.println("Digite los apellidos");
            apellidos = in.readLine();

            gestor.registrarPersonaFisica(nombre, direccion, telefono, id, apellidos);
        }

    }

    public static void registrarCliente() throws IOException {
        String nombre;
        String direccion;
        String telefono;
        String id;
        String provincia;
        String canton;
        String distrito;
        String correo;

        out.println("Digite su cedula");
        id = in.readLine();
        if (gestor.clienteExiste(id)) {
            out.println("El cliente ya existe");
        } else {
            out.println("Digite el nombre del cliente");
            nombre = in.readLine();
            out.println("Digite la direccion");
            direccion = in.readLine();
            out.println("Digite el telefono");
            telefono = in.readLine();
            out.println("Digite la provincia donde vive");
            provincia = in.readLine();
            out.println("Digite el canton donde vive");
            canton = in.readLine();
            out.println("Digite el distrito donde vive");
            distrito = in.readLine();
            out.println("Digite el correo electronico");
            correo = in.readLine();

            gestor.registrarCliente(nombre, direccion, telefono, id, provincia, canton, distrito, correo);
            gestor.crearCarro(id);
        }

    }

    public static void registrarProducto() throws IOException {
        int codigo;
        String descripcion;
        double precioOrganico;
        double precioNoOrganico;
        String desicion;
        boolean tipo;

        out.println("Digite el codigo del producto");
        codigo = Integer.parseInt(in.readLine());
        if (gestor.productoExiste(codigo)) {
            out.println("El producto ya fue registrado");
        } else {
            out.println("Digite la descripción");
            descripcion = in.readLine();
            out.println("Digite el precio Organico del producto");
            precioOrganico = Double.parseDouble(in.readLine());
            out.println("Digite el precio No Organico del producto");
            precioNoOrganico = Double.parseDouble(in.readLine());
            out.println("Digite 1 si el producto es una fruta o cualquier otra cosa si es una verdura");
            desicion = in.readLine();
            if (desicion.equals("1")) {
                tipo = true;
            } else {
                tipo = false;
            }

            gestor.registrarProducto(codigo, descripcion, precioOrganico, precioNoOrganico, tipo);
        }

    }

    public static void carritoDeCompras() throws IOException {
        int codigo;
        String id;
        boolean salir = true;
        int opcion;
        if (gestor.listarClientes().length == 0) {
            out.println("No hay clientes registrados");
        } else {
            if (gestor.listarProductos().length == 0) {
                out.println("No hay productos registrados");
            } else {
                out.println("Digite el id de cliente");
                id = in.readLine();
                if (gestor.clienteExiste(id)) {
                    do {
                        listarProductos();
                        out.println("Digite el codigo del producto que desea seleccionar");
                        codigo = Integer.parseInt(in.readLine());
                        if(gestor.productoExiste(codigo)){
                            out.println(gestor.buscarProducto(codigo));
                            out.println("1. Comprarlo");
                            out.println("2. Volver a realizar compra en mi carrito");
                            out.println("3. Volver al Menú principal");
                            opcion = Integer.parseInt(in.readLine());
                            switch (opcion) {
                                case 1:
                                    out.println("Digite la cantidad que desea comprar");
                                    int cantidad = Integer.parseInt(in.readLine());
                                    for(int i = 0 ; i<cantidad;i++){
                                        gestor.meterProducto(id, codigo);
                                    }
                                    salir = false;
                                    break;
                                case 2:
                                    carritoDeCompras();
                                    break;
                                case 3:
                                    salir = false;
                                    break;

                            }
                        }else{
                            out.println("El producto digitado no existe");
                        }

                    } while (salir);

                } else {
                    out.println("El cliente no existe");
                }


            }

        }


    }

    public static void listarProveedores() throws IOException {
        if (gestor.listarProveedores().length == 0) {
            out.println("No hay proveedores registrados");
        } else {
            for (String dato : gestor.listarProveedores()) {
                out.println(dato);
            }
        }

    }

    public static void listarClientes() throws IOException {
        if (gestor.listarClientes().length == 0) {
            out.println("No hay clientes registrados");
        } else {
            for (String dato : gestor.listarClientes()) {
                out.println(dato);
            }

        }
    }

    public static void listarProductos() throws IOException {
        if (gestor.listarProductos().length == 0) {
            out.println("No hay productos registrados");
        } else {
            for (String dato : gestor.listarProductos()) {
                out.println(dato);
            }
        }
    }

    public static void listarCarritoCompras()throws IOException{
        String id;
        out.println("Digite el id del cliente");
        id = in.readLine();
        if(gestor.clienteExiste(id)){
            out.println(gestor.listarCarro(id));
        }else{
            out.println("El cliente no existe");
        }

    }


}
